<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="67"/>
        <source>#descriptionText</source>
        <translation>&lt;p&gt;Тамагочи для ОС Аврора&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="72"/>
        <source>3-Clause BSD License</source>
        <translation>3-Clause BSD License</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="82"/>
        <source>#licenseText</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2022 io.gitlab.danyok96&lt;/i&gt;&lt;/p&gt;
&lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
&lt;ol&gt;
	&lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
	&lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
	&lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DefaultCoverPage</name>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="61"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="119"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="61"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="119"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="62"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="120"/>
        <source>, %1%2:%3h old</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="67"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="127"/>
        <source>Hunger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="77"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="151"/>
        <source>Energy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="87"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="175"/>
        <source>Happy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="97"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="199"/>
        <source>Clean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/cover/DefaultCoverPage.qml" line="107"/>
        <location filename="../qml/pages/DefaultCoverPage.qml" line="224"/>
        <source>Your pet is sick :(</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FeedMenu</name>
    <message>
        <location filename="../qml/pages/FeedMenu.qml" line="31"/>
        <source>Apple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/FeedMenu.qml" line="52"/>
        <source>Cake</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GameScreen</name>
    <message>
        <location filename="../qml/pages/GameScreen.qml" line="199"/>
        <source>End Sauna</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="153"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainScreen</name>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="62"/>
        <source>Select a Pet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="108"/>
        <source>Create new pet :)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="119"/>
        <source>Receive shared pet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="130"/>
        <source>Maximum of %1 pets reached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="136"/>
        <source>How to Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="158"/>
        <source>Create a Pet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="253"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="272"/>
        <location filename="../qml/pages/MainScreen.qml" line="388"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="304"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="304"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="304"/>
        <source>, %1:%2h old</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="329"/>
        <source>Play with this pet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="342"/>
        <source>Share pet via code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="355"/>
        <source>Share pet via email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainScreen.qml" line="371"/>
        <source>Abandon your pet :(</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuScreen</name>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="34"/>
        <source>Feed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="48"/>
        <source>Clean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="62"/>
        <source>Lights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="73"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="95"/>
        <source>Medicine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="116"/>
        <source>Sauna</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MenuScreen.qml" line="138"/>
        <source>Leave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinigameJanKenPo</name>
    <message>
        <location filename="../qml/pages/MinigameJanKenPo.qml" line="43"/>
        <source>Rock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigameJanKenPo.qml" line="59"/>
        <source>Paper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigameJanKenPo.qml" line="75"/>
        <source>Scissors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigameJanKenPo.qml" line="100"/>
        <source>Stop Playing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinigameMenu</name>
    <message>
        <location filename="../qml/pages/MinigameMenu.qml" line="31"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigameMenu.qml" line="52"/>
        <source>JanKenPo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinigamePauseOverlay</name>
    <message>
        <location filename="../qml/pages/MinigamePauseOverlay.qml" line="43"/>
        <source>PAUSED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigamePauseOverlay.qml" line="54"/>
        <source>Quit Game</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinigameSpaceShooter</name>
    <message>
        <location filename="../qml/pages/MinigameSpaceShooter.qml" line="210"/>
        <source>FIRE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinigameSpaceShooter2</name>
    <message>
        <location filename="../qml/pages/MinigameSpaceShooter2.qml" line="9"/>
        <source>Space Shooter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigameSpaceShooter2.qml" line="167"/>
        <source>Game Over</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MinigameSpaceShooter2.qml" line="471"/>
        <source>FIRE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinigameTemplate</name>
    <message>
        <location filename="../qml/pages/MinigameTemplate.qml" line="9"/>
        <source>Untitled Game</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceiveDialog</name>
    <message>
        <location filename="../qml/pages/ReceiveDialog.qml" line="60"/>
        <source>Enter code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ReceiveDialog.qml" line="94"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ReceiveDialog.qml" line="99"/>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ReceiveDialog.qml" line="111"/>
        <source>SUCCESS!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ReceiveDialog.qml" line="114"/>
        <source>Failed to connect :(</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsScreen</name>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="41"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="55"/>
        <source>Sound Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="77"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="99"/>
        <source>Retro Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="121"/>
        <source>Speed: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="151"/>
        <source>LcdColor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsScreen.qml" line="168"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShareDialog</name>
    <message>
        <location filename="../qml/pages/ShareDialog.qml" line="15"/>
        <source>Connecting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ShareDialog.qml" line="33"/>
        <source>Pet sharing code:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatusScreen</name>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="67"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="67"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="68"/>
        <source>, %1%2:%3h old</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="76"/>
        <source>Hunger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="84"/>
        <source>Energy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="92"/>
        <source>Happy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="99"/>
        <source>Clean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/StatusScreen.qml" line="106"/>
        <source>Your pet is sick :(</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TKeyboard</name>
    <message>
        <location filename="../qml/pages/TKeyboard.qml" line="20"/>
        <source>QWERTYUIOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TKeyboard.qml" line="52"/>
        <source>ASDFGHJKL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TKeyboard.qml" line="84"/>
        <source>ZXCVBNM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TKeyboard.qml" line="132"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/TKeyboard.qml" line="153"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
