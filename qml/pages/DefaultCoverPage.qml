/*******************************************************************************
**
** Copyright (C) 2022 io.gitlab.danyok96
**
** This file is part of the Тамагочи для ОС Аврора project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

Cover {
    id: coverPage
    objectName: "defaultCover"
    property int petIndexCover: gameRoot.currentPetIndex()
    BorderImage {

        id: statusScreen
        anchors.fill: parent
//        anchors.bottom: parent.bottom
        property int padding: 20

        x: parent.currentModal == "status"? padding : parent.width
        y: padding
        anchors.verticalCenter: parent.verticalCenter

        source: "qrc:/assets/res/ui/frame.png"
        width: parent.width-2*padding;
        height: parent.height
        border.left: 5; border.top: 5
        border.right: 5; border.bottom: 5

        MouseArea
        {
            id: blocker
            anchors.fill: parent
        }
        Text
        {
            id: petName
            text: gameRoot.currentPet.petName.toUpperCase()
//            anchors.bottom: spriteFrame.bottom
            anchors.top: parent.top
        }
//        TTextBitmap
//        {
//            id: petName
//            text: gameRoot.currentPet.petName.toUpperCase()
//            font: "sonic-hud-black"
//            x: 6
//            anchors.bottom: spriteFrame.bottom
//            anchors.margins: -1
//        }

        Rectangle
        {
            id: spriteFrame
            anchors.fill: sprite
            anchors.margins: -1
            color: "white"
            border.color: "black"
            radius: 2
            opacity: 0.5
        }

        PetSprite
        {
            id: sprite
            petIndex: petIndexCover
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: 7
            wander: false
            width: gameRoot.pet(petIndexCover).graphics.spriteWidth
            height: gameRoot.pet(petIndexCover).graphics.spriteHeight
        }

        Column{
            id: inColumn
            anchors.fill: parent
            anchors.margins: 6
            anchors.topMargin:  2 + petName.y + petName.height
            spacing: 2


            Text
            {
                id: ageSex
                text: (gameRoot.currentPet.male? qsTr("Male") : qsTr("Female"))+
                      qsTr(", %1%2:%3h old").arg(gameRoot.currentPet.age > 3600? Math.floor(gameRoot.currentPet.age/3600)+":" : "").arg(Math.floor(gameRoot.currentPet.age/60)%60).arg(gameRoot.currentPet.age%60)
                //anchors.horizontalCenter: parent.horizontalCenter
//                height: 2
            }

            Text
            {
                text: qsTr("Hunger")
            }

            Rectangle
            {
                width: (parent.width-6) * gameRoot.currentPet.statFood
                color: "#0044aa"
                height: 20
                y: 7
                x: 4

                //Behavior on width { PropertyAnimation {} }

                Rectangle
                {
                    color: "white"
                    height: 10
                    width: parent.width
                    opacity: 0.25
                }
            }

            Text
            {
                text: qsTr("Energy")
            }

            Rectangle
            {
                width: (parent.width-6) * gameRoot.currentPet.statEnergy
                color: "#0044aa"
                height: 20
                y: 7
                x: 4

                //Behavior on width { PropertyAnimation {} }

                Rectangle
                {
                    color: "white"
                    height: 10
                    width: parent.width
                    opacity: 0.25
                }
            }

            Text
            {
                text: qsTr("Happy")
            }

            Rectangle
            {
                width: (parent.width-6) * gameRoot.currentPet.statHappy
                color: "#0044aa"
                height: 20
                y: 7
                x: 4

                //Behavior on width { PropertyAnimation {} }

                Rectangle
                {
                    color: "white"
                    height: 10
                    width: parent.width
                    opacity: 0.25
                }
            }

            Text
            {
                text: qsTr("Clean")
            }

            Rectangle
            {
                width: (parent.width-6) * gameRoot.currentPet.statClean
                color: "#0044aa"
                height: 20
                y: 7
                x: 4

                //Behavior on width { PropertyAnimation {} }

                Rectangle
                {
                    color: "white"
                    height: 10
                    width: parent.width
                    opacity: 0.25
                }
            }

            Text
            {
                id: sickLabel
                text: qsTr("Your pet is sick :(")
                visible: gameRoot.currentPet.sick
//                visible: true
            }
        }

        Behavior on x {
            NumberAnimation{ easing.type: Easing.InOutQuad }
        }

    }
    Connections {
            target: petName
            onTextChanged : {
                     petIndexCover = gameRoot.currentPetIndex()
            }
      }


}
