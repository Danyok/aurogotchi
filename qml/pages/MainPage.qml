/*******************************************************************************
**
** Copyright (C) 2022 io.gitlab.danyok96
**
** This file is part of the Тамагочи для ОС Аврора project.
**
** Redistribution and use in source and binary forms,
** with or without modification, are permitted provided
** that the following conditions are met:
**
** * Redistributions of source code must retain the above copyright notice,
**   this list of conditions and the following disclaimer.
** * Redistributions in binary form must reproduce the above copyright notice,
**   this list of conditions and the following disclaimer
**   in the documentation and/or other materials provided with the distribution.
** * Neither the name of the copyright holder nor the names of its contributors
**   may be used to endorse or promote products derived from this software
**   without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
** THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
** IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
** OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
** PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS;
** OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
** WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE)
** ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
** EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.KeepAlive 1.2

Page {
    id: mainPage
    objectName: "mainPage"
    allowedOrientations: Orientation.All


    Component.onCompleted:
    {
//        requestActivate();

        if(gameRoot.currentPetIndex() != -1)
        {
            loadingText.visible = true;
            gameScreenLoader.sourceComponent = gameScreenComponent;
        }else
        {
            devLogoFadeIn.running = true;
        }
    }

    function res(path)
    {
//         if(Qt.platform.os == "android")
             return "qrc:/assets/res/" + path;
//         else
//             return cwd + "/res/" + path;
    }

    function petNameForType(type)
    {
        return ["generic", "devil", "cat", "snake", "bunny", "bear", "alien", "creature"][type];
    }

    Loader
    {
        id: gameScreenLoader
        anchors.fill: parent
        onStatusChanged: {
            if(status == Loader.Ready) {
                devLogo.destroy();
                faderUnfade.running = true;
            }
        }
    }

    Component
    {
        id: gameScreenComponent

        GameChrome
        {
            anchors.fill: parent
        }

    }

    Item
    {
        id: devLogo
        width: parent.width
        height: parent.height

        Rectangle
        {
            anchors.fill: parent
            color: "white"
        }

        Image
        {
            source: "qrc:/assets/res/ui/splash-dev-logo.jpg"
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }
    }

    Rectangle
    {
        id: fader
        color: "black"
        anchors.fill: parent
        visible: true
        property real speedFactor: 1

        SequentialAnimation
        {
            id: devLogoFadeIn
            running: false

            NumberAnimation { target: fader; property: "opacity"; duration: 500/fader.speedFactor; easing.type: Easing.InOutQuad; from: 1.0; to: 0.0 }
            NumberAnimation { target: fader; property: "opacity"; duration: 2000/fader.speedFactor; easing.type: Easing.InOutQuad; from: 0.0; to: 0.0 }

            ScriptAction { script: { loadingText.visible = true } }
            NumberAnimation { target: fader; property: "opacity"; duration: 500/fader.speedFactor; easing.type: Easing.InOutQuad; from: 0.0; to: 1.0 }

            ScriptAction { script: { gameScreenLoader.sourceComponent = gameScreenComponent } }

        }

        SequentialAnimation
        {
            id: faderUnfade
            running: false
            NumberAnimation { target: fader; property: "opacity"; duration: 500/fader.speedFactor; easing.type: Easing.InOutQuad; from: 1.0; to: 0.0 }
            ScriptAction { script: { fader.destroy() } }

        }

        Text
        {
          id: loadingText
          text: qsTr("Loading...")
          anchors.bottom: parent.bottom
          anchors.right: parent.right
          anchors.margins: 20
          color: "white"
          visible: false
        }
    }
    DisplayBlanking {
        id: alwaysDisplay
        preventBlanking : true
    }


    Connections {
            target: Qt.application
            onActiveChanged : {
                if( Qt.application.active ) {
                    alwaysDisplay.preventBlanking = true
//                    console.debug( "Application is active")
                }
                else {
                    alwaysDisplay.preventBlanking = false
//                    console.debug( "Application is NOT active")
                }
            }
      }

//    PageHeader {
//        objectName: "pageHeader"
//        title: qsTr("Aurogotchi")
//        extraContent.children: [
//            IconButton {
//                objectName: "aboutButton"
//                icon.source: "image://theme/icon-m-about"
//                anchors.verticalCenter: parent.verticalCenter

//                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
//            }
//        ]
//    }
}
